import pandas as pd
import random

# Load the CSV file into a DataFrame
file_path = 'connected_scales.csv'
df = pd.read_csv(file_path, delimiter=';')

# Specify the column to modify
column_to_modify = 'weight'

# Specify the percentage of values to remove
percentage_to_remove = 15  # Adjust as needed

# Calculate the number of values to remove
num_values_to_remove = int(len(df) * (percentage_to_remove / 100))

# Randomly select indices of values to remove
indices_to_remove = random.sample(range(len(df)), num_values_to_remove)

# Modify selected values in the specified column
df[column_to_modify].iloc[indices_to_remove] = None  # Replace with whatever placeholder you want

# Save the modified DataFrame back to a CSV file
output_file_path = 'connected_scales_incomplete.csv'
df.to_csv(output_file_path, index=False)
